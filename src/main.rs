use std::collections::HashMap;

use sequoia_openpgp as openpgp;
use openpgp::cert::raw::RawCertParser;
use openpgp::parse::Parse;

use anyhow::Context;
use anyhow::Result;

const TRACE: bool = false;

#[derive(Debug)]
struct Stats {
    bad_certs: usize,
    good_certs: usize,
    certs_with_non_utf8_userid: usize,
    certs_with_non_utf8_userid_creation: HashMap<usize, usize>,
    userids_with_non_utf8: usize,
    userids_with_non_utf8_tld: HashMap<String, usize>,
}

fn main() -> Result<()> {
    let mut stats = Stats {
        bad_certs: 0,
        good_certs: 0,
        certs_with_non_utf8_userid: 0,
        certs_with_non_utf8_userid_creation: Default::default(),
        userids_with_non_utf8: 0,
        userids_with_non_utf8_tld: Default::default(),
    };

    let start = std::time::SystemTime::now();
    let mut total = 0;

    for file in std::env::args().skip(1) {
        let parser = RawCertParser::from_file(&file)
            .with_context(|| {
                format!("Reading {}", file)
            })?;

        for cert in parser {
            let cert = match cert {
                Ok(cert) => cert,
                Err(err) => {
                    eprintln!("Reading a cert: {}", err);
                    stats.bad_certs += 1;
                    continue;
                }
            };

            stats.good_certs += 1;

            let mut bad = false;

            for userid in cert.userids() {
                if let Err(err) = std::str::from_utf8(userid.value()) {
                    if TRACE {
                        eprintln!("{} has a non-utf8 user ID ({}): {}",
                                  cert.fingerprint(),
                                  String::from_utf8_lossy(userid.value()),
                                  err);
                    }

                    if ! bad {
                        stats.certs_with_non_utf8_userid += 1;
                        bad = true;

                        // Get the creation year.
                        let key = cert.primary_key();
                        let creation_time = key.creation_time();
                        let creation_year = creation_time
                            .duration_since(std::time::UNIX_EPOCH)
                            .expect("valid creation time").as_secs()
                        // 365.25 days (close enough).
                            / (365 * 24 * 60 * 60 + 6 * 60 * 60) + 1970;
                        stats.certs_with_non_utf8_userid_creation
                            .entry(creation_year as usize)
                            .and_modify(|count| { *count += 1 })
                            .or_insert(1);
                    }

                    stats.userids_with_non_utf8 += 1;

                    // Get the tld.
                    let userid = userid.value();
                    if userid[userid.len() - 1] == b'>' {
                        if let Some(tld) = userid.rsplit(|c| *c == b'.').next() {
                            let tld = tld[..tld.len() - 1].to_vec();
                            if let Ok(tld) = String::from_utf8(tld) {
                                stats.userids_with_non_utf8_tld
                                    .entry(tld)
                                    .and_modify(|count| { *count += 1 })
                                    .or_insert(1);
                            }
                        }
                    }
                }
            }

            total += 1;
            if total % 2500 == 0 {
                let duration = std::time::SystemTime::now()
                    .duration_since(start)
                    .unwrap();
                eprintln!("{} certificates processed in {:?}, {:?} per certificate",
                          total, duration, duration / total);
            }
        }
    }

    println!("{:#?}", stats);

    Ok(())
}
