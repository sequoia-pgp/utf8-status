Shows stats about non-UTF-8 user IDs in a keyring:

```
$ cargo run ~/.gnupg/pubring.gpg
Stats {
    bad_certs: 0,
    good_certs: 838,
    certs_with_non_utf8_userid: 3,
    certs_with_non_utf8_userid_creation: {
        2001: 4,
        1998: 1,
    },
    userids_with_non_utf8: 5,
    userids_with_non_utf8_tld: {
        "es": 1,
        "no": 3,
    },
}
```

This was run against a dump of `keys.openpgp.org` verified keyring
(only certificates with at least one verified user ID, and only
verified user IDs) on March 25, 2024.  The results were:

```
Stats {
    bad_certs: 0,
    good_certs: 463597,
    certs_with_non_utf8_userid: 0,
    certs_with_non_utf8_userid_creation: {},
    userids_with_non_utf8: 0,
    userids_with_non_utf8_tld: {},
}
```

On reflection, it may be that `keys.openpgp.org` rejects non-UTF-8
user IDs.  On the other hand, V.B. doesn't remember receiving any
support requests about that.
